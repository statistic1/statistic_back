package ru.akaleganov.statistic.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import ru.akaleganov.statistic.domain.Role

@Repository
interface RoleRepository : JpaRepository<Role, Long?>, JpaSpecificationExecutor<Role>
