package ru.akaleganov.statistic.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Class LogingTest
 * todo: описание
 *
 * @author Kaleganov Alexander
 * @since 24 нояб. 20
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LogingTest {
}
