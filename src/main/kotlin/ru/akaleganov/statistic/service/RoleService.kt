package ru.akaleganov.statistic.service

import org.springframework.stereotype.Service
import ru.akaleganov.statistic.domain.Role
import ru.akaleganov.statistic.repository.RoleRepository

@Service
class RoleService internal constructor(private val roleRepository: RoleRepository
) {

    /**
     * получить всех пользаков c пагинацией  (гадо пдумать как реализовать)
     *
     * @param pageable пагинация
     * @return [Role]
     */
    fun findAll(): MutableCollection<Role> {
        return roleRepository.findAll()
    }
}
