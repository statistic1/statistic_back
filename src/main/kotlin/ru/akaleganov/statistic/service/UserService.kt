package ru.akaleganov.statistic.service

import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import ru.akaleganov.statistic.domain.User
import ru.akaleganov.statistic.repository.UserRepository
import ru.akaleganov.statistic.service.dto.UserDTO
import ru.akaleganov.statistic.service.dto.UserFilter
import ru.akaleganov.statistic.service.mapper.UserMapper

@Service
class UserService internal constructor(val userRepository: UserRepository,
                                       val userMapper: UserMapper,
                                       val prepareUserService: PrepareUserService) {
    private val LOGGER = LoggerFactory.getLogger(javaClass)

    /**
     * получить всех пользаков c пагинацией  (гадо пдумать как реализовать)
     *
     * @param pageable пагинация
     * @return [UserDTO]
     */
    fun findAll(criteria: UserFilter, pageable: Pageable): Page<UserDTO> {
        return userRepository.findAll(criteria.buildCriteria(), pageable).map(userMapper::userToUserDTO)
    }

    /**
     * создание пользователя через админку
     *
     * @param userDTO [UserDTO]
     * @return [UserDTO]
     */
    fun create(userDTO: UserDTO): UserDTO {
        return prepareUserService.prepareUserSave(userDTO
        ) {
            val us: User = prepareUserService.encode(userDTO)
            LOGGER.error(us.toString())
            val res: User = userRepository.save(us)
            userMapper.userToUserDTO(res)
        }
    }

    /**
     * обновление данных текущего пользователя
     *
     * @param userDTO [UserDTO]
     * @return [UserDTO]
     */
    fun updateUser(userDTO: UserDTO): UserDTO {
        return userMapper.userToUserDTO(
                    userRepository.save(
                    prepareUserService.prepareUserUpdate(userDTO))
            )
    }

    /**
     * получить пользователя по логину
     *
     * @param login логин пользователя
     * @return [UserDTO]
     */
    fun getUserByLogin(login: String?): UserDTO {
        return userMapper.userToUserDTO(userRepository.findByLogin(login).orElse(User()))
    }
}
