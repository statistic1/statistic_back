package ru.akaleganov.statistic.service

interface DispatchInterFace {
    fun initDispatch(key: String, viewService: ViewService)
}
