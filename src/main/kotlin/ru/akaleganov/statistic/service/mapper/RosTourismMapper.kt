package ru.akaleganov.statistic.service.mapper

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Service
import ru.akaleganov.statistic.service.dto.stat.RosTourismStat
import ru.akaleganov.statistic.service.dto.stat.Statistic
import java.sql.ResultSet
import java.text.SimpleDateFormat

/**
 * Ros tourism mapper
 *
 * @property dateFormat {@Link SimpleDateFormat}
 * @constructor Create empty Ros tourism mapper
 */
@Service
class RosTourismMapper(val dateFormat: SimpleDateFormat,
val  dayFormat: SimpleDateFormat) : RowMapper<Statistic> {

    override fun mapRow(p0: ResultSet, p1: Int): RosTourismStat? {
        return RosTourismStat().apply {
           p0.getTimestamp("row_date")?.let { it->
               run {
                   rowDate = dateFormat.format(it)
                   dayOfWeek = dayFormat.format(it)
               }
           }
            makeRt8 = p0.getLong("MAKE_RT8")
            rtToOther = p0.getLong("RT_to_other")
            returnRt11 = p0.getLong("Return_RT11")
            returnRt05 = p0.getLong("Return_RT05")
            makeOther = p0.getLong("Make_other")
            makeTula = p0.getLong("Make_Tula")
        }
    }
}
