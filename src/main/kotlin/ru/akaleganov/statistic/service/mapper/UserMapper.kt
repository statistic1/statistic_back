package ru.akaleganov.statistic.service.mapper

import org.springframework.stereotype.Service
import ru.akaleganov.statistic.domain.User
import ru.akaleganov.statistic.service.dto.UserDTO
import java.util.*
import java.util.stream.Collectors

/**
 * маппер для пользователей
 */
@Service
class UserMapper {
    fun usersToUsersDTOs(users: List<User>): List<UserDTO> {
        return users.stream()
                .filter { obj: User? -> Objects.nonNull(obj) }
                .map { user: User -> userToUserDTO(user) }
                .collect(Collectors.toList())
    }

    fun userToUserDTO(user: User): UserDTO {
        return UserDTO(user)
    }

    fun UsersDTOsToUsers(UsersDTOs: List<UserDTO>): MutableList<User?>? {
        return UsersDTOs.stream()
                .filter { obj: UserDTO? -> Objects.nonNull(obj) }
                .map { userDTO: UserDTO? -> userDTOToUser(userDTO) }
                .collect(Collectors.toList())
    }

    fun userDTOToUser(userDTO: UserDTO?): User {
        return if (userDTO == null) {
            User()
        } else {
            val user = User()
            user.id = userDTO.id
            user.login = userDTO.login
            user.firstName = userDTO.firstName
            user.middleName = userDTO.middleName
            user.lastName = userDTO.lastName
            user.roles = userDTO.roles
            user.pwd = userDTO.pwd
            user
        }
    }
}
