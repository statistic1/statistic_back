package ru.akaleganov.statistic.service.mapper

import org.springframework.stereotype.Service
import ru.akaleganov.statistic.domain.User
import ru.akaleganov.statistic.service.dto.AuthTokenResponseDTO
import java.util.stream.Collectors

/**
 * The type Auth token response mapper.
 */
@Service
class AuthTokenResponseMapper {
    /**
     * To dto auth token response dto.
     *
     * @param jwtToken the jwt token
     * @param users    the users
     * @return the auth token response dto
     */
    fun toDTO(jwtTokenNew: String?, users: User): AuthTokenResponseDTO {
        return AuthTokenResponseDTO().apply {
            jwtToken = jwtTokenNew
            roles = users.roles.stream().filter { it != null }
                    .map { it.name }.collect(Collectors.toList()) as List<String>?
            username = users.login
        }
    }
}
