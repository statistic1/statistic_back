package ru.akaleganov.statistic.service.mapper

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Service
import ru.akaleganov.statistic.service.dto.stat.RnbStat
import ru.akaleganov.statistic.service.dto.stat.Statistic
import java.sql.ResultSet
import java.text.SimpleDateFormat

/**
 * Ros tourism mapper
 *
 * @property dateFormat {@Link SimpleDateFormat}
 * @constructor Create empty Ros tourism mapper
 */
@Service
class RnbMapper(val dateFormat: SimpleDateFormat,
                val  dayFormat: SimpleDateFormat) : RowMapper<Statistic> {

    override fun mapRow(p0: ResultSet, p1: Int): RnbStat? {
        return RnbStat().apply {
           p0.getTimestamp("row_date")?.let { it->
               run {
                   rowDate = dateFormat.format(it)
                   dayOfWeek = dayFormat.format(it)
               }
           }
            countAll = p0.getLong("CountAll")
            cans = p0.getLong("cANS")
            cabn = p0.getLong("cABN")
            sumTalkTime = p0.getLong("sumTalkTime")
            avgTalTime = p0.getBigDecimal("avgTalTime")
            callToBank = p0.getBigDecimal("cCallToBank")
            sl = p0.getLong("SL")
            percABN = p0.getLong("percABN")
        }
    }
}
