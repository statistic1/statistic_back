package ru.akaleganov.statistic.service.dto

import ru.akaleganov.statistic.domain.Role
import ru.akaleganov.statistic.domain.User
import java.io.Serializable
import java.util.*

/**
 * ДТО для ответа и для изменений данных пользователя
 */
class UserDTO : Serializable {
    var id: Long? = null
    var login: String? = null
    var lastName: String? = null
    var firstName: String? = null
    var middleName: String? = null
    var pwd: String? = null
    var errorMessage: String? = null
        private set

    fun setErrorMessage(errorMessage: String?): UserDTO {
        this.errorMessage = errorMessage
        return this
    }

    public var roles: List<Role> = ArrayList()

    constructor(user: User?) {
        user?.let {
            id = user.id
            login = user.login
            lastName = user.lastName
            firstName = user.firstName
            middleName = user.middleName
            roles = user.roles
        }

    }

    constructor() {}

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val userDTO = o as UserDTO
        return id == userDTO.id &&
                login == userDTO.login &&
                lastName == userDTO.lastName &&
                firstName == userDTO.firstName &&
                middleName == userDTO.middleName &&
                pwd == userDTO.pwd &&
                roles == userDTO.roles
    }

    override fun hashCode(): Int {
        return Objects.hash(id, login, lastName, firstName, middleName, pwd, roles)
    }

    override fun toString(): String {
        return "UserDTO{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", pwd='" + pwd + '\'' +
                ", roles=" + roles +
                '}'
    }
}
