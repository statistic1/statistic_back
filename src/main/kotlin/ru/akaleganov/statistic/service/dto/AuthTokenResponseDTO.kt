package ru.akaleganov.statistic.service.dto

import ru.akaleganov.statistic.domain.User

/**
 * ответ когда пользователь авторизован
 *
 */
class AuthTokenResponseDTO {
    /**
     * токен
     */
    var jwtToken: String? = null

    /**
     * имя пользователя
     */
    var username: String? = null
    var roles: List<String>? = null

}
