package ru.akaleganov.statistic.service.dto.stat

import java.time.DayOfWeek

/**
 * Ros tourism stat
 *
 * @property date
 * @property makeRt8
 * @property RT_to_other
 * @property Return_RT11
 * @property Return_RT05
 * @property Make_other
 * @property Make_Tula
 * @constructor Create empty Ros tourism stat
 */
data class RosTourismStat (
        /**
         * дата
         */
        var rowDate: String? = "",
        var dayOfWeek: String? = "",
        var makeRt8: Long? = 0,
        var rtToOther: Long? = 0,
        var returnRt11: Long? = 0,
        var returnRt05: Long? = 0,
        var makeOther: Long? = 0,
        var makeTula: Long? = 0): Statistic {
}
