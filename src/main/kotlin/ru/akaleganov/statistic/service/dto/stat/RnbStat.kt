package ru.akaleganov.statistic.service.dto.stat

import java.math.BigDecimal
import java.time.DayOfWeek

/**
 * Ros tourism stat
 *
 * @property date
 * @property makeRt8
 * @property RT_to_other
 * @property Return_RT11
 * @property Return_RT05
 * @property Make_other
 * @property Make_Tula
 * @constructor Create empty Ros tourism stat
 */
data class RnbStat (
        /**
         * дата
         */
        var rowDate: String? = "",
        var dayOfWeek: String? = "",
        var countAll: Long? =0,
        var cans: Long? =0,
        var cabn: Long? =0,
        var sumTalkTime: Long? =0,
        var avgTalTime: BigDecimal? =BigDecimal(0),
        var callToBank: BigDecimal? = BigDecimal(0),
        var sl: Long? =0,
        var percABN: Long? =0,
       ): Statistic {
}
