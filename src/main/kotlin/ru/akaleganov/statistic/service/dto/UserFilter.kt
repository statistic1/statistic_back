package ru.akaleganov.statistic.service.dto

import org.springframework.data.jpa.domain.Specification
import ru.akaleganov.statistic.domain.User
import java.io.Serializable
import java.util.*
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Root

class UserFilter : Serializable {
    var login: String? = null
    var firstName: String? = null
    fun buildCriteria(): Specification<User?>? {
        var criteria: Specification<User?>? = null
        if (login != null) {
            criteria = Specification { user: Root<User?>, cq: CriteriaQuery<*>?, cb: CriteriaBuilder -> cb.equal(user.get<Any>("login"), login) }
        }
        if (firstName != null) {
            criteria = criteria?.and { user: Root<User?>, cq: CriteriaQuery<*>?, cb: CriteriaBuilder -> cb.equal(user.get<Any>("firstName"), firstName) }
                    ?: Specification { user: Root<User?>, cq: CriteriaQuery<*>?, cb: CriteriaBuilder -> cb.equal(user.get<Any>("firstName"), firstName) }
        }
        return criteria
    }


    override fun toString(): String {
        return "UserFilter{" +
                "login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                '}'
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserFilter

        if (login != other.login) return false
        if (firstName != other.firstName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = login?.hashCode() ?: 0
        result = 31 * result + (firstName?.hashCode() ?: 0)
        return result
    }
}
