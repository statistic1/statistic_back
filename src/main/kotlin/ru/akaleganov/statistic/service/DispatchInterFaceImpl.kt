package ru.akaleganov.statistic.service

import org.springframework.stereotype.Service

@Service
class DispatchInterFaceImpl : DispatchInterFace {
    val dispatch = HashMap<String, ViewService>()
    override fun initDispatch(key: String, viewService: ViewService) {
        dispatch[key] = viewService;
    }

}
