package ru.akaleganov.statistic.service.viewimpl

import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component
import ru.akaleganov.statistic.service.ViewService
import ru.akaleganov.statistic.service.dto.stat.Statistic
import ru.akaleganov.statistic.service.filterUtil.FilterSQL
import ru.akaleganov.statistic.service.mapper.RnbMapper
import java.text.SimpleDateFormat

@Component
class RbnSkill40ViewService(
        val jdbcTemplate: JdbcTemplate,
       val rbnMapper: RnbMapper,
        val sqlCombiner: SqlCombiner) : ViewService {
    private val LOGGER = LoggerFactory.getLogger(javaClass)

    /**
     * Get all view
     *
     * @return
     */
    override fun getStatistic(filterSQL: FilterSQL): MutableList<Statistic> =
            jdbcTemplate.query(
                    sqlCombiner.createSql(
                            StringBuilder("SELECT * FROM vrusnarbank40"), filterSQL),
                    rbnMapper)

    override fun getKey() = "rbnSkill40"
}
