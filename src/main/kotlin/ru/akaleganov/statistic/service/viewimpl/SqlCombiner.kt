package ru.akaleganov.statistic.service.viewimpl

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import ru.akaleganov.statistic.service.filterUtil.FilterSQL

@Service
class SqlCombiner {
    private val log = LoggerFactory.getLogger(SqlCombiner::class.java)

    fun createSql(sql: StringBuilder, filterSQL: FilterSQL) =
            sql.apply {
                if (filterSQL.dateStart != null || filterSQL.dateEnd != null) {
                    append(" where ")
                }
                filterSQL.dateStart?.let {
                    append("  row_date >= '")
                    append(it)
                    append("' ")
                    filterSQL.dateEnd?.let { append(" and ") }
                }
                filterSQL.dateEnd?.let {
                    append(" row_date <= '")
                    append(it)
                    append("'")
                }
                log.info("sql = {}", toString())
            }.toString()
}
