package ru.akaleganov.statistic.service.viewimpl

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component
import ru.akaleganov.statistic.service.ViewService
import ru.akaleganov.statistic.service.dto.stat.Statistic
import ru.akaleganov.statistic.service.filterUtil.FilterSQL
import ru.akaleganov.statistic.service.mapper.RnbMapper
import java.text.SimpleDateFormat

/**
 * Rbn skill41view service
 * имплементация для сбора отчёта РНБ Скилл 41
 * @property jdbcTemplate
 * @property dateFormat
 * @property sqlCombiner
 * @constructor Create empty Rbn skill41view service
 */
@Component
class RbnSkill41ViewService(
        val jdbcTemplate: JdbcTemplate,
        val dateFormat: SimpleDateFormat,
        val dayFormat: SimpleDateFormat,
        val sqlCombiner: SqlCombiner) : ViewService {
    /**
     * Get all view
     *
     * @return
     */
    override fun getStatistic(filterSQL: FilterSQL): MutableList<Statistic> =
            jdbcTemplate.query(
                    sqlCombiner.createSql(StringBuilder("SELECT * FROM vrusnarbank41"), filterSQL),
                    RnbMapper(dateFormat, dayFormat))

    override fun getKey() = "rbnSkill41"
}
