package ru.akaleganov.statistic.service.viewimpl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component
import ru.akaleganov.statistic.service.dto.stat.Statistic
import ru.akaleganov.statistic.service.ViewService
import ru.akaleganov.statistic.service.filterUtil.FilterSQL
import ru.akaleganov.statistic.service.mapper.RosTourismMapper
import java.text.SimpleDateFormat

@Component
class RosTourismViewService(
    val jdbcTemplate: JdbcTemplate,
    val rosTourismMapper: RosTourismMapper,
    val sqlCombiner: SqlCombiner
) : ViewService {
    /**
     * Get all view
     *
     * @return
     */
    override fun getStatistic(filterSQL: FilterSQL): MutableList<Statistic> = jdbcTemplate.query(
        sqlCombiner.createSql(
            StringBuilder("SELECT * FROM vros_tourism2"),
            filterSQL
        ),
        rosTourismMapper
    )

    override fun getKey(): String {
        return "rosTourism"
    }
}
