package ru.akaleganov.statistic.service

import org.slf4j.LoggerFactory
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import ru.akaleganov.statistic.domain.User
import ru.akaleganov.statistic.repository.UserRepository
import ru.akaleganov.statistic.service.dto.UserDTO
import ru.akaleganov.statistic.service.mapper.UserMapper
import java.util.function.Supplier

/**
 * сервис для рефакторинга и подготовки пользователя для сохранения
 */
@Service
class PrepareUserService(private val userRepository: UserRepository,
                         private val bCryptPasswordEncoder: BCryptPasswordEncoder,
                         private val userMapper: UserMapper) {
    /**
     * сохранение  пользователя в бд
     *
     * @param save функция сохранения которую надо реализовать
     * @return [UserDTO]
     */
    fun prepareUserSave(userDTO: UserDTO, save: Supplier<UserDTO>): UserDTO {
        return if (userDTO.login == findUserByLogin(userDTO.login).login) {
            userDTO.setErrorMessage("Пользователь с таким логином уже есть в бд")
        } else {
            save.get()
        }
    }

    /**
     * подготовка пользователя к обновлению (проверка паролей)
     *
     * @param userDTO [UserDTO]
     * @return [User]
     */
    fun prepareUserUpdate(userDTO: UserDTO): User {
        return if (userDTO.pwd != null && userDTO.pwd != "") {
            encode(userDTO)
        } else {
            userDTO.pwd = findUserByLogin(userDTO.login).pwd
            userMapper.userDTOToUser(userDTO)
        }
    }

    /**
     * @param userDTO [UserDTO]
     * @return [User]
     */
    fun encode(userDTO: UserDTO): User {
        if (userDTO.pwd != null) {
            userDTO.pwd = bCryptPasswordEncoder.encode(userDTO.pwd)
        }
        LOGGER.info(userDTO.toString())
        return userMapper.userDTOToUser(userDTO)
    }

    /**
     * получить пользователя по логину
     *
     * @param login логин пользователя
     * @return [User]
     */
    private fun findUserByLogin(login: String?): User {
        return userRepository.findByLogin(login).orElse(User())
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(PrepareUserService::class.java)
    }
}
