package ru.akaleganov.statistic.service.filterUtil


class FilterSQL(val filterName: String? = null,
                val dateStart: String? = null,
                val dateEnd: String? = null ) {
    override fun toString(): String {
        return "FilterSQL(filterName=$filterName, dateStart=$dateStart, dateEnd=$dateEnd)"
    }
}
