package ru.akaleganov.statistic.service.filterUtil

interface Criteria {
    fun copy(): Criteria?
}
