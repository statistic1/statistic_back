package ru.akaleganov.statistic.service.filterUtil

import java.io.Serializable
import java.util.*

/**
 * Base class for the various attribute filters. It can be added to a criteria class as a member, to support the
 * following query parameters:
 * <pre>
 * fieldName.equals='something'
 * fieldName.specified=true
 * fieldName.specified=false
 * fieldName.notEquals='somethingElse'
 * fieldName.in='something','other'
</pre> *
 */
open class Filter<FIELD_TYPE> : Serializable {
    /**
     *
     * Getter for the field `equals`.
     *
     * @return a FIELD_TYPE object.
     */
    var equals: FIELD_TYPE? = null
         set

    /**
     *
     * Getter for the field `notEquals`.
     *
     * @return a FIELD_TYPE object.
     */
    var notEquals: FIELD_TYPE? = null
        private set

    /**
     *
     * Getter for the field `specified`.
     *
     * @return a [Boolean] object.
     */
     var specified: Boolean? = null
         set

    /**
     *
     * Getter for the field `in`.
     *
     * @return a [List] object.
     */
    var `in`: List<FIELD_TYPE>? = null
         set

    /**
     *
     * Constructor for Filter.
     */
    constructor() {}

    /**
     *
     * Constructor for Filter.
     *
     */
    constructor(filter: Filter<FIELD_TYPE>) {
        equals = filter.equals
        notEquals = filter.notEquals
        specified = filter.specified
        `in` = if (filter.`in` == null) null else ArrayList(filter.`in`)
    }

    /**
     *
     * copy.
     *
     */
    open fun copy(): Filter<FIELD_TYPE>? {
        return Filter(this)
    }

    /**
     *
     * Setter for the field `equals`.
     *
     * @param equals a FIELD_TYPE object.
     */
    fun setEquals(equals: FIELD_TYPE): Filter<FIELD_TYPE> {
        this.equals = equals
        return this
    }

    /**
     *
     * Setter for the field `notEquals`.
     *
     * @param notEquals a FIELD_TYPE object.
     */
    fun setNotEquals(notEquals: FIELD_TYPE): Filter<FIELD_TYPE> {
        this.notEquals = notEquals
        return this
    }

    /**
     *
     * Setter for the field `specified`.
     *
     * @param specified a [Boolean] object.
     */
    fun setSpecified(specified: Boolean?): Filter<FIELD_TYPE> {
        this.specified = specified
        return this
    }

    /**
     *
     * Setter for the field `in`.
     *
     * @param in a [List] object.
     */
    fun setIn(`in`: List<FIELD_TYPE>?): Filter<FIELD_TYPE> {
        this.`in` = `in`
        return this
    }

    /** {@inheritDoc}  */
    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || javaClass != o.javaClass) {
            return false
        }
        val filter = o as Filter<*>
        return equals == filter.equals &&
                notEquals == filter.notEquals &&
                specified == filter.specified &&
                `in` == filter.`in`
    }

    /** {@inheritDoc}  */
    override fun hashCode(): Int {
        return Objects.hash(equals, notEquals, specified, `in`)
    }

    /** {@inheritDoc}  */
    override fun toString(): String {
        return (filterName + " ["
                + (if (equals != null) "equals=$equals, " else "")
                + (if (notEquals != null) "notEquals=$notEquals, " else "")
                + (if (`in` != null) "in=" + `in` + ", " else "")
                + (if (specified != null) "specified=$specified" else "")
                + "]")
    }

    /**
     *
     * getFilterName.
     *
     * @return a [String] object.
     */
    protected val filterName: String
        protected get() = this.javaClass.simpleName

    companion object {
        private const val serialVersionUID = 1L
    }
}
