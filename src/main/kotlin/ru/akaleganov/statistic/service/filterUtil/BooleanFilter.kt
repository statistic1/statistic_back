package ru.akaleganov.statistic.service.filterUtil

class BooleanFilter : Filter<Boolean?> {
    constructor() {}
    constructor(filter: BooleanFilter) : super(filter) {}

    override fun copy(): BooleanFilter {
        return BooleanFilter(this)
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
