package ru.akaleganov.statistic.service.filterUtil

class LongFilter(filter: LongFilter) : RangeFilter<Long>(filter) {

    override fun copy(): LongFilter {
        return LongFilter(this)
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
