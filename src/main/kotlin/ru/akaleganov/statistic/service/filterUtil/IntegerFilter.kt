package ru.akaleganov.statistic.service.filterUtil

class IntegerFilter : RangeFilter<Int> {
    constructor() {}
    constructor(filter: IntegerFilter) : super(filter) {}

    override fun copy(): IntegerFilter {
        return IntegerFilter(this)
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
