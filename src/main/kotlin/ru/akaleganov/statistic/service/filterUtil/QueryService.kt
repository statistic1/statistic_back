package ru.akaleganov.statistic.service.filterUtil

import org.springframework.data.jpa.domain.Specification
import org.springframework.transaction.annotation.Transactional
import java.util.function.Function
import javax.persistence.criteria.*
import javax.persistence.metamodel.SetAttribute
import javax.persistence.metamodel.SingularAttribute

/**
 * Base service for constructing and executing complex queries.
 *
 * @param <ENTITY> the type of the entity which is queried.
</ENTITY> */
@Transactional(readOnly = true)
abstract class QueryService<ENTITY> {
    /**
     * Helper function to return a specification for filtering on a single field, where equality, and null/non-null
     * conditions are supported.
     *
     * @param filter the individual attribute filter coming from the frontend.
     * @param field  the JPA static metamodel representing the field.
     * @param <X>    The type of the attribute which is filtered.
     * @return a Specification
    </X> */
    protected fun <X> buildSpecification(filter: Filter<X?>, field: SingularAttribute<in ENTITY, X>?): Specification<ENTITY>? {
        return buildSpecification(filter) { root: Root<ENTITY> -> root[field] }
    }

    /**
     * Helper function to return a specification for filtering on a single field, where equality, and null/non-null
     * conditions are supported.
     *
     * @param filter            the individual attribute filter coming from the frontend.
     * @param metaclassFunction the function, which navigates from the current entity to a column, for which the filter applies.
     * @param <X>               The type of the attribute which is filtered.
     * @return a Specification
    </X> */
    protected fun <X> buildSpecification(filter: Filter<X?>, metaclassFunction: Function<Root<ENTITY>, Expression<X>>): Specification<ENTITY>? {
        if (filter.equals != null) {
            return equalsSpecification(metaclassFunction, filter.equals)
        } else if (filter.`in` != null) {
            return valueIn(metaclassFunction, filter.`in`)
        } else if (filter.notEquals != null) {
            return notEqualsSpecification(metaclassFunction, filter.notEquals)
        } else if (filter.specified != null) {
            return byFieldSpecified(metaclassFunction, filter.specified!!)
        }
        return null
    }

    /**
     * Helper function to return a specification for filtering on a [String] field, where equality, containment,
     * and null/non-null conditions are supported.
     *
     * @param filter the individual attribute filter coming from the frontend.
     * @param field  the JPA static metamodel representing the field.
     * @return a Specification
     */
    protected fun buildStringSpecification(filter: StringFilter, field: SingularAttribute<in ENTITY, String>?): Specification<ENTITY>? {
        return buildSpecification(filter) { root: Root<ENTITY> -> root.get(field) }
    }

    /**
     * Helper function to return a specification for filtering on a [String] field, where equality, containment,
     * and null/non-null conditions are supported.
     *
     * @param filter            the individual attribute filter coming from the frontend.
     * @param metaclassFunction lambda, which based on a Root&lt;ENTITY&gt; returns Expression - basicaly picks a column
     * @return a Specification
     */
    protected fun buildSpecification(filter: StringFilter, metaclassFunction: Function<Root<ENTITY>, Expression<String?>>): Specification<ENTITY>? {
        if (filter.equals != null) {
            return equalsSpecification(metaclassFunction, filter.equals)
        } else if (filter.`in` != null) {
            return valueIn(metaclassFunction, filter.`in`)
        } else if (filter.contains != null) {
            return likeUpperSpecification(metaclassFunction, filter.contains)
        } else if (filter.doesNotContain != null) {
            return doesNotContainSpecification(metaclassFunction, filter.doesNotContain)
        } else if (filter.notEquals != null) {
            return notEqualsSpecification(metaclassFunction, filter.notEquals)
        } else if (filter.specified != null) {
            return byFieldSpecified(metaclassFunction, filter.specified!!)
        }
        return null
    }

    /**
     * Helper function to return a specification for filtering on a single [Comparable], where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported.
     *
     * @param <X>    The type of the attribute which is filtered.
     * @param filter the individual attribute filter coming from the frontend.
     * @param field  the JPA static metamodel representing the field.
     * @return a Specification
    </X> */
    protected fun <X : Comparable<X>> buildRangeSpecification(filter: RangeFilter<X>,
                                                               field: SingularAttribute<in ENTITY, X>?): Specification<ENTITY> {
        return buildSpecification(filter) { root: Root<ENTITY> -> root[field] }
    }

    /**
     * Helper function to return a specification for filtering on a single [Comparable], where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported.
     *
     * @param <X>               The type of the attribute which is filtered.
     * @param filter            the individual attribute filter coming from the frontend.
     * @param metaclassFunction lambda, which based on a Root&lt;ENTITY&gt; returns Expression - basicaly picks a column
     * @return a Specification
    </X> */
    protected fun <X : Comparable<X>> buildSpecification(filter: RangeFilter<X>,
                                                          metaclassFunction: Function<Root<ENTITY>, Expression<X>>): Specification<ENTITY> {
        if (filter.equals != null) {
            return equalsSpecification(metaclassFunction, filter.equals)
        } else if (filter.`in` != null) {
            return valueIn(metaclassFunction, filter.`in`)
        }
        var result: Specification<ENTITY> = Specification.where(null)
        if (filter.specified != null) {
            result =
                result.and(byFieldSpecified(metaclassFunction, filter.specified!!))
            }
        if (filter.notEquals != null) {
            result = result.and(notEqualsSpecification(metaclassFunction, filter.notEquals))
        }
        if (filter.getGreaterThan() != null) {
            result = result.and(greaterThan(metaclassFunction, filter.getGreaterThan()))
        }
        if (filter.getGreaterThanOrEqual() != null) {
            result = result.and(greaterThanOrEqualTo(metaclassFunction, filter.getGreaterThanOrEqual()))
        }
        if (filter.getLessThan() != null) {
            result = result.and(lessThan(metaclassFunction, filter.getLessThan()))
        }
        if (filter.getLessThanOrEqual() != null) {
            result = result.and(lessThanOrEqualTo(metaclassFunction, filter.getLessThanOrEqual()))
        }
        return result
    }

    /**
     * Helper function to return a specification for filtering on one-to-one or many-to-one reference. Usage:
     * <pre>
     * Specification&lt;Employee&gt; specByProjectId = buildReferringEntitySpecification(criteria.getProjectId(),
     * Employee_.project, Project_.id);
     * Specification&lt;Employee&gt; specByProjectName = buildReferringEntitySpecification(criteria.getProjectName(),
     * Employee_.project, Project_.name);
    </pre> *
     *
     * @param filter     the filter object which contains a value, which needs to match or a flag if nullness is
     * checked.
     * @param reference  the attribute of the static metamodel for the referring entity.
     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>    The type of the referenced entity.
     * @param <X>        The type of the attribute which is filtered.
     * @return a Specification
    </X></OTHER> */
    protected fun <OTHER, X> buildReferringEntitySpecification(filter: Filter<X?>,
                                                               reference: SingularAttribute<in ENTITY, OTHER>?,
                                                               valueField: SingularAttribute<in OTHER, X>?): Specification<ENTITY>? {
        return buildSpecification(filter) { root: Root<ENTITY> -> root[reference][valueField] }
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference. Usage:
     * <pre>
     * Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(criteria.getEmployeId(),
     * Project_.employees, Employee_.id);
     * Specification&lt;Employee&gt; specByEmployeeName = buildReferringEntitySpecification(criteria.getEmployeName(),
     * Project_.project, Project_.name);
    </pre> *
     *
     * @param filter     the filter object which contains a value, which needs to match or a flag if emptiness is
     * checked.
     * @param reference  the attribute of the static metamodel for the referring entity.
     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>    The type of the referenced entity.
     * @param <X>        The type of the attribute which is filtered.
     * @return a Specification
    </X></OTHER> */
    protected fun <OTHER, X> buildReferringEntitySpecification(filter: Filter<X?>,
                                                               reference: SetAttribute<ENTITY, OTHER>,
                                                               valueField: SingularAttribute<OTHER, X>?): Specification<ENTITY>? {
        return buildReferringEntitySpecification(filter, { root: Root<ENTITY> -> root.join(reference) }) { entity: SetJoin<ENTITY, OTHER> -> entity[valueField] }
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference.Usage:<pre>
     * Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(
     * criteria.getEmployeId(),
     * root -&gt; root.get(Project_.company).join(Company_.employees),
     * entity -&gt; entity.get(Employee_.id));
     * Specification&lt;Employee&gt; specByProjectName = buildReferringEntitySpecification(
     * criteria.getProjectName(),
     * root -&gt; root.get(Project_.project)
     * entity -&gt; entity.get(Project_.name));
    </pre> *
     *
     * @param filter           the filter object which contains a value, which needs to match or a flag if emptiness is
     * checked.
     * @param functionToEntity the function, which joins he current entity to the entity set, on which the filtering is applied.
     * @param entityToColumn   the function, which of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>          The type of the referenced entity.
     * @param <MISC>           The type of the entity which is the last before the OTHER in the chain.
     * @param <X>              The type of the attribute which is filtered.
     * @return a Specification
    </X></MISC></OTHER> */
    protected fun <OTHER, MISC, X> buildReferringEntitySpecification(filter: Filter<X?>,
                                                                     functionToEntity: Function<Root<ENTITY>, SetJoin<MISC, OTHER>>,
                                                                     entityToColumn: Function<SetJoin<MISC, OTHER>, Expression<X>>?): Specification<ENTITY>? {
        if (filter.equals != null) {
            return equalsSpecification(functionToEntity.andThen(entityToColumn), filter.equals)
        } else if (filter.specified != null) {
            // Interestingly, 'functionToEntity' doesn't work, we need the longer lambda formula
            return byFieldSpecified({ root: Root<ENTITY> -> functionToEntity.apply(root) },
                    filter.specified!!)
        }
        return null
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference.Where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported. Usage:
     * <pre>
     * Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(criteria.getEmployeId(),
     * Project_.employees, Employee_.id);
     * Specification&lt;Employee&gt; specByEmployeeName = buildReferringEntitySpecification(criteria.getEmployeName(),
     * Project_.project, Project_.name);
    </pre> *
     *
     * @param <X>        The type of the attribute which is filtered.
     * @param filter     the filter object which contains a value, which needs to match or a flag if emptiness is
     * checked.
     * @param reference  the attribute of the static metamodel for the referring entity.
     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>    The type of the referenced entity.
     * @return a Specification
    </OTHER></X> */
    protected fun <OTHER, X : Comparable<X>> buildReferringEntitySpecification(filter: RangeFilter<X>,
                                                                                reference: SetAttribute<ENTITY, OTHER>,
                                                                                valueField: SingularAttribute<OTHER, X>?): Specification<ENTITY> {
        return buildReferringEntitySpecification(filter, { root: Root<ENTITY> -> root.join(reference) }) { entity: SetJoin<ENTITY, OTHER> -> entity[valueField] }
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference.Where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported. Usage:
     * <pre>`
     * Specification<Employee> specByEmployeeId = buildReferringEntitySpecification(
     * criteria.getEmployeId(),
     * root -> root.get(Project_.company).join(Company_.employees),
     * entity -> entity.get(Employee_.id));
     * Specification<Employee> specByProjectName = buildReferringEntitySpecification(
     * criteria.getProjectName(),
     * root -> root.get(Project_.project)
     * entity -> entity.get(Project_.name));
    ` *
    </pre> *
     *
     * @param <X>              The type of the attribute which is filtered.
     * @param filter           the filter object which contains a value, which needs to match or a flag if emptiness is
     * checked.
     * @param functionToEntity the function, which joins he current entity to the entity set, on which the filtering is applied.
     * @param entityToColumn   the function, which of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>          The type of the referenced entity.
     * @param <MISC>           The type of the entity which is the last before the OTHER in the chain.
     * @return a Specification
    </MISC></OTHER></X> */
    protected fun <OTHER, MISC, X : Comparable<X>>
            buildReferringEntitySpecification(filter: RangeFilter<X>,
                                              functionToEntity: Function<Root<ENTITY>, SetJoin<MISC, OTHER>>,
                                                                                      entityToColumn: Function<SetJoin<MISC, OTHER>, Expression<X>>?): Specification<ENTITY> {
        val fused = functionToEntity.andThen(entityToColumn)
        if (filter.equals != null) {
            return equalsSpecification(fused, filter.equals)
        } else if (filter.`in` != null) {
            return valueIn(fused, filter.`in`)
        }
        var result: Specification<ENTITY> = Specification.where(null)
        if (filter.specified != null) {
            // Interestingly, 'functionToEntity' doesn't work, we need the longer lambda formula
            result = result.and(byFieldSpecified({
                root: Root<ENTITY> -> functionToEntity.apply(root) }, filter.specified!!))
        }
        if (filter.notEquals != null) {
            result = result.and(notEqualsSpecification(fused, filter.notEquals))
        }
        if (filter.getGreaterThan() != null) {
            result = result.and(greaterThan(fused, filter.getGreaterThan()))
        }
        if (filter.getGreaterThanOrEqual() != null) {
            result = result.and(greaterThanOrEqualTo(fused, filter.getGreaterThanOrEqual()))
        }
        if (filter.getLessThan() != null) {
            result = result.and(lessThan(fused, filter.getLessThan()))
        }
        if (filter.getLessThanOrEqual() != null) {
            result = result.and(lessThanOrEqualTo(fused, filter.getLessThanOrEqual()))
        }
        return result
    }

    /**
     * Generic method, which based on a Root&lt;ENTITY&gt; returns an Expression which type is the same as the given 'value' type.
     *
     * @param metaclassFunction function which returns the column which is used for filtering.
     * @param value             the actual value to filter for.
     * @param <X>              The type of the attribute which is filtered.
     * @return a Specification.
    </X> */
    protected fun <X> equalsSpecification(metaclassFunction: Function<Root<ENTITY>, Expression<X>>, value: X?): Specification<ENTITY> {
        return Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.equal(metaclassFunction.apply(root), value) }
    }

    /**
     * Generic method, which based on a Root&lt;ENTITY&gt; returns an Expression which type is the same as the given 'value' type.
     *
     * @param metaclassFunction function which returns the column which is used for filtering.
     * @param value             the actual value to exclude for.
     * @param <X>              The type of the attribute which is filtered.
     * @return a Specification.
    </X> */
    protected fun <X> notEqualsSpecification(metaclassFunction: Function<Root<ENTITY>, Expression<X>>, value: X?): Specification<ENTITY> {
        return Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.not(builder.equal(metaclassFunction.apply(root), value)) }
    }

    /**
     *
     * likeUpperSpecification.
     *
     * @param metaclassFunction a [Function] object.
     * @param value a [String] object.
     * @return a [Specification] object.
     */
    protected fun likeUpperSpecification(metaclassFunction: Function<Root<ENTITY>, Expression<String?>>,
                                         value: String?): Specification<ENTITY> {
        return Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.like(builder.upper(metaclassFunction.apply(root)), wrapLikeQuery(value)) }
    }

    /**
     *
     * doesNotContainSpecification.
     *
     * @param metaclassFunction a [Function] object.
     * @param value a [String] object.
     * @return a [Specification] object.
     */
    protected fun doesNotContainSpecification(metaclassFunction: Function<Root<ENTITY>, Expression<String?>>,
                                              value: String?): Specification<ENTITY> {
        return Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.not(builder.like(builder.upper(metaclassFunction.apply(root)), wrapLikeQuery(value))) }
    }

    /**
     *
     * byFieldSpecified.
     *
     * @param metaclassFunction a [Function] object.
     * @param specified a boolean.
     * @param <X> a X object.
     * @return a [Specification] object.
    </X> */
    protected fun <X> byFieldSpecified(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                       specified: Boolean): Specification<ENTITY> {
        return if (specified) Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?,
                                              builder: CriteriaBuilder ->
            builder.isNotNull(metaclassFunction.apply(root)) } else
            Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?,
                            builder: CriteriaBuilder -> builder.isNull(metaclassFunction.apply(root)) }
    }

    /**
     *
     * byFieldEmptiness.
     *
     * @param metaclassFunction a [Function] object.
     * @param specified a boolean.
     * @param <X> a X object.
     * @return a [Specification] object.
    </X> */
    protected fun <X> byFieldEmptiness(metaclassFunction: Function<Root<ENTITY>?, Expression<Set<X>?>?>,
                                       specified: Boolean): Specification<ENTITY> {
        return if (specified) Specification { root: Root<ENTITY>?, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.isNotEmpty(metaclassFunction.apply(root)) } else Specification { root: Root<ENTITY>?, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.isEmpty(metaclassFunction.apply(root)) }
    }

    /**
     *
     * valueIn.
     *
     * @param metaclassFunction a [Function] object.
     * @param values a [Collection] object.
     * @param <X> a X object.
     * @return a [Specification] object.
    </X> */
    protected fun <X> valueIn(
        metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
        values: Collection<X?>?): Specification<ENTITY> {
        return Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?, builder: CriteriaBuilder ->
            var `in` = builder.`in`(metaclassFunction.apply(root))
            for (value in values!!) {
                `in` = `in`.value(value)
            }
            `in`
        }
    }

    /**
     *
     * greaterThanOrEqualTo.
     *
     * @param metaclassFunction a [Function] object.
     * @param value a X object.
     * @param <X> a X object.
     * @return a [Specification] object.
    </X> */
    protected fun <X : Comparable<X>?> greaterThanOrEqualTo(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                                            value: X?): Specification<ENTITY> {
        return Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.greaterThanOrEqualTo(metaclassFunction.apply(root), value) }
    }

    /**
     *
     * greaterThan.
     *
     * @param metaclassFunction a [Function] object.
     * @param value a X object.
     * @param <X> a X object.
     * @return a [Specification] object.
    </X> */
    protected fun <X : Comparable<X>?> greaterThan(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                                   value: X?): Specification<ENTITY> {
        return Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.greaterThan(metaclassFunction.apply(root), value) }
    }

    /**
     *
     * lessThanOrEqualTo.
     *
     * @param metaclassFunction a [Function] object.
     * @param value a X object.
     * @param <X> a X object.
     * @return a [Specification] object.
    </X> */
    protected fun <X : Comparable<X>?> lessThanOrEqualTo(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                                         value: X?): Specification<ENTITY> {
        return Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.lessThanOrEqualTo(metaclassFunction.apply(root), value) }
    }

    /**
     *
     * lessThan.
     *
     * @param metaclassFunction a [Function] object.
     * @param value a X object.
     * @param <X> a X object.
     * @return a [Specification] object.
    </X> */
    protected fun <X : Comparable<X>?> lessThan(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                                value: X?): Specification<ENTITY> {
        return Specification { root: Root<ENTITY>, query: CriteriaQuery<*>?, builder: CriteriaBuilder -> builder.lessThan(metaclassFunction.apply(root), value) }
    }

    /**
     *
     * wrapLikeQuery.
     *
     * @param txt a [String] object.
     * @return a [String] object.
     */
    protected fun wrapLikeQuery(txt: String?): String {
        return "%" + txt!!.toUpperCase() + '%'
    }
}
