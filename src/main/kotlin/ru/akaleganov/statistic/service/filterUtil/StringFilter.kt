package ru.akaleganov.statistic.service.filterUtil

import java.util.*

class StringFilter : Filter<String?> {
    var contains: String? = null
        private set
    var doesNotContain: String? = null
        private set

    constructor() {}
    constructor(filter: StringFilter) : super(filter) {
        contains = filter.contains
        doesNotContain = filter.doesNotContain
    }

    override fun copy(): StringFilter {
        return StringFilter(this)
    }

    fun setDoesNotContain(doesNotContain: String?): StringFilter {
        this.doesNotContain = doesNotContain
        return this
    }

    fun setContains(contains: String?): StringFilter {
        this.contains = contains
        return this
    }

    override fun equals(o: Any?): Boolean {
        return if (this === o) {
            true
        } else if (o != null && this.javaClass == o.javaClass) {
            if (!super.equals(o)) {
                false
            } else {
                val that = o as StringFilter
                contains == that.contains && doesNotContain == that.doesNotContain
            }
        } else {
            false
        }
    }

    override fun hashCode(): Int {
        return Objects.hash(*arrayOf<Any?>(super.hashCode(), contains, doesNotContain))
    }

    override fun toString(): String {
        return this.filterName + " [" + (if (contains != null) "contains=" + contains + ", " else "") + (if (doesNotContain != null) "doesNotContain=" + doesNotContain + ", " else "") + (if (equals != null) "equals=" + equals + ", " else "") + (if (notEquals != null) "notEquals=" + notEquals + ", " else "") + (if (specified != null) "specified=" + specified else "") + "]"
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
