package ru.akaleganov.statistic.service.filterUtil

import java.util.*

open class RangeFilter<FIELD_TYPE : Comparable<FIELD_TYPE>> : Filter<FIELD_TYPE> {
    private lateinit var greaterThan: FIELD_TYPE
    private lateinit var lessThan: FIELD_TYPE
    private lateinit var greaterThanOrEqual: FIELD_TYPE
    private lateinit var lessThanOrEqual: FIELD_TYPE

    constructor()
    constructor(filter: RangeFilter<FIELD_TYPE>) : super(filter) {
        greaterThan = filter.greaterThan
        lessThan = filter.lessThan
        greaterThanOrEqual = filter.greaterThanOrEqual
        lessThanOrEqual = filter.lessThanOrEqual
    }

    override fun copy(): RangeFilter<FIELD_TYPE> {
        return RangeFilter(this)
    }

    fun getGreaterThan(): FIELD_TYPE? {
        return greaterThan
    }

    fun setGreaterThan(greaterThan: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.greaterThan = greaterThan
        return this
    }

    fun getGreaterThanOrEqual(): FIELD_TYPE? {
        return greaterThanOrEqual
    }

    fun setGreaterThanOrEqual(greaterThanOrEqual: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.greaterThanOrEqual = greaterThanOrEqual
        return this
    }

    @Deprecated("")
    fun setGreaterOrEqualThan(greaterThanOrEqual: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.greaterThanOrEqual = greaterThanOrEqual
        return this
    }

    fun getLessThan(): FIELD_TYPE? {
        return lessThan
    }

    fun setLessThan(lessThan: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.lessThan = lessThan
        return this
    }

    fun getLessThanOrEqual(): FIELD_TYPE? {
        return lessThanOrEqual
    }

    fun setLessThanOrEqual(lessThanOrEqual: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.lessThanOrEqual = lessThanOrEqual
        return this
    }

    @Deprecated("")
    fun setLessOrEqualThan(lessThanOrEqual: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.lessThanOrEqual = lessThanOrEqual
        return this
    }

    override fun equals(o: Any?): Boolean {
        return if (this === o) {
            true
        } else if (o != null && this.javaClass == o.javaClass) {
            if (!super.equals(o)) {
                false
            } else {
                val that = o as RangeFilter<*>
                greaterThan == that.greaterThan && lessThan == that.lessThan && greaterThanOrEqual == that.greaterThanOrEqual && lessThanOrEqual == that.lessThanOrEqual
            }
        } else {
            false
        }
    }

    override fun hashCode(): Int {
        return Objects.hash(*arrayOf<Any>(super.hashCode(),
                greaterThan, lessThan, greaterThanOrEqual, lessThanOrEqual))
    }

    override fun toString(): String {
        return this.filterName + " [" + (if (getGreaterThan() != null) "greaterThan=" + getGreaterThan() + ", " else "") + (if (getGreaterThanOrEqual() != null) "greaterThanOrEqual=" + getGreaterThanOrEqual() + ", " else "") + (if (getLessThan() != null) "lessThan=" + getLessThan() + ", " else "") + (if (getLessThanOrEqual() != null) "lessThanOrEqual=" + getLessThanOrEqual() + ", " else "") + (if (equals != null)
            "equals=" + equals + ", " else "") +
                (if (specified != null) "specified=" + specified + ", " else "") + (if
                                                                                            (`in` != null) "in=" + `in` else "") + "]"
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
