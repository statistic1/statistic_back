package ru.akaleganov.statistic.service

import org.springframework.beans.factory.annotation.Autowired
import ru.akaleganov.statistic.service.dto.stat.Statistic
import ru.akaleganov.statistic.service.filterUtil.FilterSQL

/**
 * View service
 *
 * @param R тип данных по которым будет собрана статистика
 * @constructor Create empty View service
 */
interface ViewService {
    fun getKey() :String
    @Autowired
    fun initMap( dispatchInterFace: DispatchInterFace) {
        dispatchInterFace.initDispatch(getKey(), this)
    }
    fun getStatistic(filterSQL: FilterSQL): MutableIterable<Statistic>
}
