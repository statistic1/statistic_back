package ru.akaleganov.statistic.domain

import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.MappedSuperclass

/**
 * The type Mapped super class.
 */
@MappedSuperclass
open class MappedSuperClass
/**
 * Instantiates a new Mapped super class.
 */
{
    /**
     * Gets id.
     *
     * @return the id
     */
    /**
     * Sets id.
     *
     * @param id the id
     */
    /**
     * идентификатор объекта в бд
     */
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    var id: Long? = null
}
