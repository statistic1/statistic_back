package ru.akaleganov.statistic.domain

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList

@Entity(name = "user_1")
class User : MappedSuperClass(), UserDetails {
    @Column(name = "login")
    var login: String? = null

    @Column(name = "last_name")
    var lastName: String? = null

    @Column(name = "first_name")
    var firstName: String? = null

    @Column(name = "middle_name")
    var middleName: String? = null

    @Column(name = "pwd")
    var pwd: String? = null

    @ManyToMany(cascade = [CascadeType.MERGE], fetch = FetchType.EAGER)
    @JoinTable(name = "user_1_role", joinColumns = [JoinColumn(name = "user_1_id")], inverseJoinColumns = [JoinColumn(name = "role_id")])
    var roles: List<Role> = ArrayList();



    override fun getAuthorities(): Collection<GrantedAuthority> {
        return ArrayList<GrantedAuthority>(roles)
    }

    override fun getPassword(): String {
        return pwd!!
    }

    override fun getUsername(): String {
        return login!!
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }



    override fun toString(): String {
        return "User{" +
                "login='" + login + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", pwd='" + pwd + '\'' +
                ", roles=" + roles +
                '}'
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (login != other.login) return false
        if (lastName != other.lastName) return false
        if (firstName != other.firstName) return false
        if (middleName != other.middleName) return false
        if (pwd != other.pwd) return false
        if (roles != other.roles) return false

        return true
    }

    override fun hashCode(): Int {
        var result = login?.hashCode() ?: 0
        result = 31 * result + (lastName?.hashCode() ?: 0)
        result = 31 * result + (firstName?.hashCode() ?: 0)
        result = 31 * result + (middleName?.hashCode() ?: 0)
        result = 31 * result + (pwd?.hashCode() ?: 0)
        result = 31 * result + roles.hashCode()
        return result
    }
}
