package ru.akaleganov.statistic.domain

import org.springframework.security.core.GrantedAuthority
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity

@Entity(name = "role")
class Role : MappedSuperClass, GrantedAuthority {
    @Column(name = "name")
    var name: String? = null

    constructor() {}
    constructor(id: Long?) {
        this.id = id
    }

    constructor(id: Long?, name: String?) {
        this.id = id
        this.name = name
    }

    constructor(name: String?) {
        this.name = name
    }

    override fun getAuthority(): String {
        return name!!
    }



    override fun toString(): String {
        return "Role{" +
                "name='" + name + '\'' +
                '}'
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Role

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name?.hashCode() ?: 0
    }
}
