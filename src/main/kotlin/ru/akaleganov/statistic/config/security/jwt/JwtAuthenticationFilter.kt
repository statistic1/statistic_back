package ru.akaleganov.statistic.config.security.jwt

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils.hasText
import org.springframework.web.filter.OncePerRequestFilter
import ru.akaleganov.statistic.config.security.UserDetailServiceCustom
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * This class checks for the authorization header
 * and authenticates the JWT token and sets the authentication
 * in the context.Doing so will protect our APIs from those
 * requests which do not have any authorization token.
 */
@Component
class JwtAuthenticationFilter(@Autowired private val jwtTokenUtil: JwtTokenUtil,
                              private val userDetailServiceCustom: UserDetailServiceCustom,
                              @Value("\${jwt.header}")
                              private val HEADER_STRING: String
) : OncePerRequestFilter() {
    private val log = LoggerFactory.getLogger(JwtAuthenticationFilter::class.java)
    val AUTHORIZATION = "Authorization"


    @Throws(AuthenticationException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        logger.info("do filter...");
        val token = getTokenFromRequest(request)
        if (token != null && jwtTokenUtil.validateToken(token)) {
            val userLogin = jwtTokenUtil.getUsernameFromToken(token);
            val customUserDetails = userDetailServiceCustom.loadUserByUsername(userLogin)
            val auth =  UsernamePasswordAuthenticationToken(customUserDetails, null, customUserDetails.authorities)
            SecurityContextHolder.getContext().authentication = auth;
        } else {
           log.info("токен не найден")
        }
        chain.doFilter(request, response);
    }

    fun  getTokenFromRequest( request: HttpServletRequest): String? {
        val bearer = request.getHeader(AUTHORIZATION);
        if (hasText(bearer) && bearer.startsWith("Bearer ")) {
            return bearer.substring(7);
        }
        return null;
    }
}
