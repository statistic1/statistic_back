package ru.akaleganov.statistic.config.security

import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import ru.akaleganov.statistic.domain.User
import ru.akaleganov.statistic.repository.UserRepository

@Service
class UserDetailServiceCustom( val userRepository: UserRepository) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(login: String?): User {
        val users = userRepository.findByLogin(login).orElse(User())
        if (users.login == null) {
            throw UsernameNotFoundException(login)
        }
        return users
    }
}
