package ru.akaleganov.statistic.config.security.jwt

import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import java.io.Serializable
import java.util.*
import java.util.function.Function


/**
 * Service for working with jwt tokken
 */
@Service
class JwtTokenUtil(@Value("\${jwt.secret}")
                   private val SECRET_KEY: String,
                   @Value("\${jwt.sessionTime}")
                   private val SESSION_TIME: Long) : Serializable {
    private val log = LoggerFactory.getLogger(JwtTokenUtil::class.java)


    fun getUsernameFromToken(token: String): String {
        return getClaimFromToken(token) { obj: Claims -> obj.subject }
    }

    fun isTokenExpired(token: String): Boolean {
        val expiration = getExpirationDateFromToken(token)
        return expiration.before(Date())
    }

    private fun getExpirationDateFromToken(token: String): Date {
        return getClaimFromToken(token) { obj: Claims -> obj.expiration }
    }

    private fun <T> getClaimFromToken(token: String, claimsResolver: Function<Claims, T>): T {
        log.debug("All claims will be")
        val claims = getAllClaimsFromToken(token)
        log.debug("All claims {}:", claims)
        return claimsResolver.apply(claims)
    }

    private fun getAllClaimsFromToken(token: String): Claims {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .body
    }

    fun doGenerateToken(user: UserDetails): String {
        val claims = Jwts.claims().setSubject(user.username)
        claims["authorities"] = user.authorities
        return Jwts.builder()
                .setClaims(claims)
                .setIssuer("akaleganov")
                .setIssuedAt(Date(System.currentTimeMillis()))
                .setExpiration(Date(System.currentTimeMillis() + SESSION_TIME))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact()
    }

    fun validateToken(token: String?): Boolean {
        try {
            Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token)
            return true
        } catch (expEx: ExpiredJwtException) {
            log.error("Token expired")
        } catch (unsEx: UnsupportedJwtException) {
            log.error("Unsupported jwt")
        } catch (mjEx: MalformedJwtException) {
            log.error("Malformed jwt")
        } catch (sEx: SignatureException) {
            log.error("Invalid signature")
        } catch (e: Exception) {
            log.error("invalid token")
        }
        return false
    }
}
