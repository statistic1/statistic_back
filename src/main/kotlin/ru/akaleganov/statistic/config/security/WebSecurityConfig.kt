package ru.akaleganov.statistic.config.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import ru.akaleganov.statistic.config.security.jwt.JwtAuthenticationEntryPoint
import ru.akaleganov.statistic.config.security.jwt.JwtAuthenticationFilter

/**
 * The type Web security config.
 */
@Configuration
@EnableWebSecurity
class WebSecurityConfig(@Autowired
                        val authenticationTokenFilterBean: JwtAuthenticationFilter,
                        private var unauthorizedHandler: JwtAuthenticationEntryPoint
)
    : WebSecurityConfigurerAdapter() {


    @Autowired
    @Throws(Exception::class)
    protected fun configureGlobal(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService<UserDetailsService?>(userDetailServiceCustom).passwordEncoder(encoder())
    }

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Autowired
    private val userDetailServiceCustom: UserDetailServiceCustom? = null



    @Bean
    fun encoder(): BCryptPasswordEncoder? {
        return BCryptPasswordEncoder()
    }


    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.cors()
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/*", "/index.html", "/index.html**").permitAll()
                .antMatchers("/api/auth").permitAll()
                .antMatchers("/api/auth/registry").permitAll()
                .antMatchers("/api/auth/roles/").authenticated()
                .antMatchers("/api/roles").authenticated()
                .antMatchers("/api/**").authenticated()
                .antMatchers("/api/users/**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/url/**").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/api/url").hasAnyAuthority("ADMIN", "USER")
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        http.addFilterBefore(authenticationTokenFilterBean, UsernamePasswordAuthenticationFilter::class.java)
    }

}
