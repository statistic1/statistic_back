package ru.akaleganov.statistic.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.text.SimpleDateFormat
import javax.sql.DataSource
import liquibase.integration.spring.SpringLiquibase


/**
 * Custom bean
 *
 * @constructor Create empty Custom bean
 */
@Configuration
class CustomBean {

    @Bean
    fun dateFormat() = SimpleDateFormat("yyyy-MM-dd")
    @Bean
    fun dayFormat() = SimpleDateFormat("E")

    @Bean
    fun liquibase(dataSourceNew: DataSource?): SpringLiquibase {
        return SpringLiquibase().apply {
            dataSource = dataSourceNew
            changeLog = "classpath:db/changelog-master.xml"
            contexts = "dev, prod"
            setShouldRun(true)
        }
    }
}
