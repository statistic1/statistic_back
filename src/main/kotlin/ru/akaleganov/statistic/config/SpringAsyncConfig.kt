package ru.akaleganov.statistic.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import ru.akaleganov.statistic.config.security.SpringWebConfig


@Configuration
@EnableAsync
class SpringAsyncConfig {
    /**
     * Web mvc configurer
     *
     * @return
     */
    @Bean
    protected fun webMvcConfigurer(): WebMvcConfigurer? {
        return object : SpringWebConfig() {
            override fun configureAsyncSupport(configurer: AsyncSupportConfigurer) {
                configurer.setTaskExecutor(taskExecutor())
            }
        }
    }

    /**
     * Task executor
     *
     */
    @Bean
    fun taskExecutor() = ThreadPoolTaskExecutor().apply {
        corePoolSize = 5
        maxPoolSize = 10
        setQueueCapacity(3000)
    }
}
