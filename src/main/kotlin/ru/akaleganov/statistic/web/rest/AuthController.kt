package ru.akaleganov.statistic.web.rest

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.web.bind.annotation.*
import ru.akaleganov.statistic.config.security.UserDetailServiceCustom
import ru.akaleganov.statistic.config.security.jwt.JwtTokenUtil
import ru.akaleganov.statistic.domain.Role
import ru.akaleganov.statistic.domain.User
import ru.akaleganov.statistic.service.UserService
import ru.akaleganov.statistic.service.dto.AuthTokenResponseDTO
import ru.akaleganov.statistic.service.dto.UserDTO
import ru.akaleganov.statistic.service.mapper.AuthTokenResponseMapper
import java.security.Principal
import java.util.logging.Logger

/**
 * The type Auth controller.
 */
@RestController
@RequestMapping("/api")
class AuthController(private val authenticationManager: AuthenticationManager,
                     val jwtTokenUtil: JwtTokenUtil,
                     val userDetailServiceCustom: UserDetailServiceCustom,
                     val authTokenResponseMapper: AuthTokenResponseMapper,
                     val userService: UserService,
                    ) {
    private val LOGGER = LoggerFactory.getLogger(javaClass)

    /**
     * Register response entity.
     *
     * @param loginUser the login user
     * @return the response entity
     */
    @PostMapping("/auth")
    fun register(@RequestBody loginUser: User): ResponseEntity<AuthTokenResponseDTO> {
        LOGGER.debug("SIGN IN REQUEST:")
        val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        loginUser.login,
                        loginUser.password
                )
        )
        SecurityContextHolder.getContext().authentication = authentication
        val user: UserDetails = userDetailServiceCustom.loadUserByUsername(loginUser.login)
        val token: String = jwtTokenUtil.doGenerateToken(user)
        return ResponseEntity.ok(authTokenResponseMapper.toDTO(token,
                userDetailServiceCustom.loadUserByUsername(loginUser.login)))
    }

    /**
     * Gets roles this user.
     *
     * @param principal the principal
     * @return the roles this user
     */
    @GetMapping(value = ["/auth/roles"])
    fun getRolesThisUser(principal: Principal): ResponseEntity<List<Role>> {
        return ResponseEntity.ok().body(userService.getUserByLogin(principal.name).roles)
    }

}
