package ru.akaleganov.statistic.web.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import ru.akaleganov.statistic.service.dto.stat.Statistic
import ru.akaleganov.statistic.service.DispatchInterFaceImpl
import ru.akaleganov.statistic.service.filterUtil.FilterSQL

@RestController
@RequestMapping("/api")
class StatController(
        @Autowired val dispatchInterFaceImpl: DispatchInterFaceImpl
) {
    @GetMapping(path = ["/findStatistic"])
    @ResponseBody
    fun findStatisticRosTourism(filterSQL: FilterSQL): Flux<Statistic>? =
            filterSQL.let {
                dispatchInterFaceImpl.dispatch[filterSQL.filterName]?.let {
                    Flux.fromIterable(
                            it.getStatistic(filterSQL))
                }
            }

}
