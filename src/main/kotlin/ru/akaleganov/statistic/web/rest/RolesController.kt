package ru.akaleganov.statistic.web.rest

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import ru.akaleganov.statistic.domain.Role
import ru.akaleganov.statistic.service.RoleService

@RestController
@RequestMapping("/api")
class RolesController(private val roleService: RoleService) {


    /**
     *
     * @return получить список всех ролей
     */
    @GetMapping("/roles")
    fun findAllRoles(): Flux<Role> {
        return Flux.fromIterable(roleService.findAll())
    }
}
