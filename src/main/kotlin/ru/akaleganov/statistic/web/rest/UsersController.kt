package ru.akaleganov.statistic.web.rest

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ru.akaleganov.statistic.service.UserService
import ru.akaleganov.statistic.service.dto.UserDTO
import ru.akaleganov.statistic.service.dto.UserFilter

@RestController
@RequestMapping("/api")
class UsersController(val userService: UserService) {
    @GetMapping(value = ["/users"])
    fun getAllUsers(criteria: UserFilter, pageable: Pageable): ResponseEntity<List<UserDTO>> {
        val page: Page<UserDTO> = userService.findAll(criteria, pageable)
        val headers = HttpHeaders()
        headers.add("Access-Control-Expose-Headers", "totalSize")
        headers["totalSize"] = java.lang.Long.toString(page.getTotalElements())
        return ResponseEntity.ok().headers(headers).body<List<UserDTO>>(page.getContent())
    }

    @PostMapping(value = ["/users"])
    fun createUser(@RequestBody user: UserDTO): ResponseEntity<UserDTO> {
        return ResponseEntity.ok().body(userService.create(user))
    }

    @PutMapping(value = ["/users"])
    fun updateUser(@RequestBody user: UserDTO): ResponseEntity<UserDTO> {
        return ResponseEntity.ok().body(userService.updateUser(user))
    }

}
